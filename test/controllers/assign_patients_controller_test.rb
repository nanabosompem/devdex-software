require 'test_helper'

class AssignPatientsControllerTest < ActionController::TestCase
  setup do
    @assign_patient = assign_patients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assign_patients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create assign_patient" do
    assert_difference('AssignPatient.count') do
      post :create, assign_patient: { patient_id: @assign_patient.patient_id, role_id: @assign_patient.role_id, status: @assign_patient.status }
    end

    assert_redirected_to assign_patient_path(assigns(:assign_patient))
  end

  test "should show assign_patient" do
    get :show, id: @assign_patient
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @assign_patient
    assert_response :success
  end

  test "should update assign_patient" do
    patch :update, id: @assign_patient, assign_patient: { patient_id: @assign_patient.patient_id, role_id: @assign_patient.role_id, status: @assign_patient.status }
    assert_redirected_to assign_patient_path(assigns(:assign_patient))
  end

  test "should destroy assign_patient" do
    assert_difference('AssignPatient.count', -1) do
      delete :destroy, id: @assign_patient
    end

    assert_redirected_to assign_patients_path
  end
end
