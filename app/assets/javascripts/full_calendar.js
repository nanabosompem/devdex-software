
$(document).ready(function() {


  $('.calendar').each(function(){
    var calendar = $(this);
    calendar.fullCalendar({

       header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      selectable: true,
      selectHelper: true,
      editable: true,
      eventLimit: true,
      events: '/appointments.json',


        select: function() {
        $.getScript('/appointments/new', function() {});
        $('.calendar').fullCalendar('unselect');
      },

     eventClick: function(appointment, jsEvent, view) {
        $.getScript(appointment.edit_url, function() {});
      }
      

    });
  })

  });