json.extract! incident_report, :id, :incident_type, :incidate_date, :incident_description, :action_taken, :status, :created_at, :updated_at
json.url incident_report_url(incident_report, format: :json)
