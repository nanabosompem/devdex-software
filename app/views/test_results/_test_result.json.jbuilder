json.extract! test_result, :id, :patient_id, :user_id, :status, :results, :created_at, :updated_at
json.url test_result_url(test_result, format: :json)
