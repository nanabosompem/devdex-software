json.extract! order_master, :id, :order_name, :status, :created_at, :updated_at
json.url order_master_url(order_master, format: :json)
