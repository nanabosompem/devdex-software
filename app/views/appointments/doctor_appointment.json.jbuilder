


json.array! @doctor_appointments do |appointment|
  date_format = appointment.all_day_event? ? '%Y-%m-%d' : '%Y-%m-%dT%H:%M:%S'
 

json.id appointment.id
json.title appointment.description
json.doctor_id appointment.doctor_id
json.patient_id appointment.patient_id
json.start appointment.app_time.strftime(date_format)
json.end appointment.app_time.strftime(date_format)
json.color appointment.color unless appointment.color.blank?
json.allDay appointment.all_day_event? ? true : false
json.update_url appointment_path(appointment, method: :patch)
json.edit_url edit_appointment_path(appointment)

end



