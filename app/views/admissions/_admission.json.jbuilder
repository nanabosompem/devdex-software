json.extract! admission, :id, :patient_id, :admission_note, :user_id, :personnel_id, :status, :created_at, :updated_at
json.url admission_url(admission, format: :json)
