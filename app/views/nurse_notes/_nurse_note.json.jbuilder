json.extract! nurse_note, :id, :notes, :user_id, :time, :patient_id, :status, :created_at, :updated_at
json.url nurse_note_url(nurse_note, format: :json)
