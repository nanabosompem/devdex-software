class ResultsPdf <Prawn::Document
  def initialize (document1)

    super( top_margin: 100)
    @documents = document1

    heading
    line_items
    
  end

  def heading

    text "THE TEST RESULTS ARE: ", size: 20,  style: :bold
  end

  def line_items
    move_down 20

    text "#{@documents.results} ", size: 15,  style: :italic
  # table line_item_rows do
  # 	row(0).font_style= :bold
  # 	columns(1).align= :right
  # 	self.row_colors = ["DDDDDD","FFFFFF"]
  # 	self.header =true
  # end
  end

  def line_item_rows

    [["Test Results"]] +
    @documents.each do |item|
      [item.results]
    end
  end

end