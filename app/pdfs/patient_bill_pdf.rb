class PatientBillPdf <Prawn::Document
  def initialize (document1)

    super( top_margin: 100)
    @bill_histories = document1

    heading
    line_items
    
  end

  def heading

    text "Medication Bills : ", size: 10,  style: :bold
  end

  def line_items
    move_down 10

    #text "#{@bill_histories} ", size: 15,  style: :italic
  table line_item_rows do
    row(0).font_style= :bold
    columns(1).align= :right
    self.row_colors = ["DDDDDD","FFFFFF"]
    self.header =true
  end
  end

  def line_item_rows

  # [["Order"]] + [["Price"]] + [["Date"]] 
 
    [["Name"]+["Order"]+["Total Cost (GHC)"]] + 
    @bill_histories.map do |item|
      [item.other_names + " " + item.surname] +
      [item.service_name] + [item.price]
       
    end
  end

end