class AdmissionsController < ApplicationController
  before_action :set_admission, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :admission_doc, :admission_vitals]
  # require './lib/push/notify.rb' 
 
  
  def index

    @admission = Admission.new
    @add_a_room = Admission.new
    @a_discharge_note = Admission.new
    @clinic_document = ClinicDocument.new
    
    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id INNER JOIN users ON users.id = admissions.user_id").select('card_no,surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').paginate(page: params[:page], per_page: 20).where(personnel_id: current_user.role_id,status: 1)
  
  if params[:count]
      params[:count]
    else
      params[:count] = 20 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 20
    end

    @per_page = params[:per_page] || Admission.per_page || 20

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 20
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)

    if params[:search_value] && params[:search_value].strip != ''
      if params[:search_param] == 'name'
        @admissions = Admission.name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      end
    elsif params[:search_param] == 'date'
      #start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
      #ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"

      #@patients = Patient.date_search(start,ended).paginate(page: params[:page], per_page: params[:count]).order('created_at asc')

    else
    end  
  
  
  
  
   end

  def nurse_admission_list
    @admission = Admission.new
    @add_a_room = Admission.new
    @a_discharge_note = Admission.new

    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 1)
  end

  def discharged_list

    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('card_no,surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 0)
  end
  
  def discharged_list_nurses
    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.room_no').where(status: 0)
 
  end

  # GET /admissions/1
  # GET /admissions/1.json
  def show
  end

  # GET /admissions/new
  def new
    @admission = Admission.new
  end

  # GET /admissions/1/edit
  def edit
  end

  def admission_doc
    @a_discharge_note = Admission.new

    @admission = Admission.new
    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)
    @admissions = Admission.joins("INNER JOIN patients ON patients.id = admissions.patient_id
                    INNER JOIN users ON users.id = admissions.user_id").select('surname,other_names,admission_note,admissions.patient_id,fullname,admissions.created_at,admissions.status').where(patient_id: patient_id )
  end

  def patient_admission

    @admission = Admission.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

    respond_to do |format|

      format.js
      format.html
    end
  end

  def view_note

    @admission = Admission.new
    created_at=params[:id]

    @documents = Admission.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_discharge_note

    @admission = Admission.new
    created_at=params[:id]

    @documents = Admission.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def add_room

    @add_a_room = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"

  end
  
  def edit_room

    @add_a_room = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)
    @room_no = @admission_records.room_no
    logger.info "LETS SEE ROom no #{@room_no}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"

  end

  def add_a_room

    @room_no = params[:admission][:room_no]
    @created_at = params[:admission][:created_at]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"
    logger.info "LETS SEE THE ROom number #{@room_no}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"
    logger.info "LETS SEE Admission ID #{@admission_records.id}"

    Admission.update(@admission_records.id, :room_no => @room_no)

    respond_to do |format|

      format.html  { redirect_to admissions_path, notice: 'Room number was added successfully.' }
      format.js
    end

  end

  # def discharge_patient
  #   created_at =params[:id]
  #   @admission_records = Admission.find_by(created_at: created_at)

  #   if Admission.update(@admission_records.id, :status => 0)
  #     redirect_to admissions_path, notice: 'Patient Discharged.'

  #   end
  # end

  def discharge_note
    @a_discharge_note = Admission.new
    @created_at=params[:id]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"

    @admission_records = Admission.find_by(created_at: @created_at)

    logger.info "LETS SEE Admission object #{@admission_records}"
    logger.info "LETS SEE Admission object #{@admission_records.inspect}"
  end

  def a_discharge_note
    @discharge_note = params[:admission][:discharge_note]
    @created_at = params[:admission][:created_at]

    logger.info "LETS SEE THE DATE SELECTED #{@created_at}"
    logger.info "LETS SEE THE ROom number #{@discharge_note}"

    @admission_records = Admission.find_by(created_at: @created_at)

    Admission.update(@admission_records.id, :discharge_note => @discharge_note,:status => 0)

    redirect_to :back
    # respond_to do |format|
    #   redirect_to :back

    #   # format.html  { redirect_to admissions_path, notice: 'Patient Discharged Successfully.' }
    #   format.js
    # end
  end

  def admission_vitals

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"
    if (defined?(patient_id)).nil?

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{patient_id}"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id,status: 0 ).order('id desc')

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(id: patient_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else

      logger.info "XXXXXXXXXXXXXXXXXXXXXX"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id,status: 0 ).order('id desc')

    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def doctor_admission_vitals_doc

    @clinic_document = ClinicDocument.new

    patient_id=params[:id]
    @patient_id = patient_id

    @b_records = BasicMedRecord.where(patient_id: patient_id).order('id desc')
     
    @blood_pressure = @b_records[0].blood_pressure
    @pulse = @b_records[0].pulse
    @respiratory = @b_records[0].respiratory_rate
    @temperature =@b_records[0].temp
    @patient_id = @b_records[0].patient_id
    @height  = @b_records[0].height
    @weight  = @b_records[0].weight
    @saturation  = @b_records[0].saturation



      @clinic_records = ClinicDocument.where(patient_id: patient_id).order('id desc')
     if @clinic_records.exists?
     @complaints = @clinic_records[0].complaints
     @history_complaints = @clinic_records[0].history_complaints
     @direct_question = @clinic_records[0].direct_question
     @systems_review = @clinic_records[0].systems_review
     @med_history = @clinic_records[0].med_history
     @social_history = @clinic_records[0].social_history
     @allergies = @clinic_records[0].allergies
       @clinic_exams  = @clinic_records[0].clinic_exams
       @final_diagnosis  = @clinic_records[0].final_diagnosis
         @investigations  = @clinic_records[0].investigations
         @treatment_plans  = @clinic_records[0].treatment_plans
      logger.info "Lets see clinic_records #{@clinic_records.inspect}"
      logger.info "Lets see complaints #{@complaints}"
     else
     end
     
     

    @vital_signs = "Respiratory Rate: " + @b_records[0].respiratory_rate + " " + "Pulse: " +  @b_records[0].pulse + " " + "Blood Pressure: " +  @b_records[0].blood_pressure + " " + "Temperature: " +  @b_records[0].temp + " " + "Oxygen saturation: " +  @b_records[0].saturation.to_s + " " + "Weight: " +  @b_records[0].weight.to_s + " " + "Height: " +  @b_records[0].height.to_s

    logger.info "Vital signs: #{@vital_signs}"

    if (defined?(@b_records)).nil?
      @ass_p_records = AssignPatient.find_by(patient_id: patient_id)

      AssignPatient.update(@ass_p_records.id, :status => 1)

      logger.info "------------------------------------------"
      logger.info "Lets see the b records"
      logger.info "basic medical record: #{@b_records.inspect}"
      logger.info "------------------------------------------"

      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id
      logger.info "patient id: #{@patient_id}"
      logger.info " basic_med_id: #{@basic_med_id}"
      logger.info "   @b_records.blood_pressure: #{ @blood_pressure}"

    else

      logger.info " LIEEEEESSSSS"

    end

    respond_to do |format|

      format.js
      format.html
    end
  end

  def view_doctor_admission_doc

    @assign_patient = AssignPatient.new
    created_at=params[:id]

    @documents = ClinicDocument.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  # POST /admissions
  # POST /admissions.json
  def create
    patient_id =  params[:admission][:patient_id]
    admission_days = params[:admission][:admission_days]
    logger.info "Lets see PATIENT ID #{patient_id}"
    logger.info "Lets see ADMISION DAYS #{admission_days}"

    service_id = 8
    @services_data = ServicesInventory.find_by(id: service_id)
    price = @services_data.price.to_f * admission_days.to_i
    logger.info "Lets see PRICE #{price}"

    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :quantity => admission_days, :price => price ,:status=> 0 , :delete_status => 0})
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
    @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :quantity => admission_days, :price => price ,:status=> 0,:delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end

    @admission = Admission.new(admission_params)

    respond_to do |format|
      if @admission.save
        format.html { redirect_to admission_doc_path(:id => patient_id), notice: 'Admission was successfully created.' }
        format.json { render :show, status: :created, location: @admission }



         #NOTIFICATION TO CASHIER
        #  notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
        #  logger.info "NOTIFCATIONS --------"
        #   push =  Push::Notify.single_notification(notification_for_user,"Admission Bill Generated")
        #   logger.info push
        #   logger.info "ENDS--------"
      else
        format.html { render :new }
        format.json { render json: @admission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admissions/1
  # PATCH/PUT /admissions/1.json
  def update
    respond_to do |format|
      if @admission.update(admission_params)
        format.html { redirect_to admissions_path, notice: 'Admission was successfully updated.' }
        format.json { render :show, status: :ok, location: @admission }
      else
        format.html { render :edit }
        format.json { render json: @admission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admissions/1
  # DELETE /admissions/1.json
  def destroy
    @admission.destroy
    respond_to do |format|
      format.html { redirect_to admissions_url, notice: 'Admission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_admission
    @admission = Admission.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admission_params
    params.require(:admission).permit(:patient_id, :admission_note, :user_id, :personnel_id, :status, :room_no, :admission_days)
  end
end
