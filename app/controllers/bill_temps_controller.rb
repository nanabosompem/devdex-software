class BillTempsController < ApplicationController
  before_action :set_bill_temp, only: [:show, :edit, :update, :destroy]
  # GET /bill_temps
  # GET /bill_temps.json
  def index
    #@bill_temps = BillTemp.all

    @bill_temps = BillTemp.joins("INNER JOIN patients ON patients.id = bill_temps.patient_id
                    ").select('card_no,surname,other_names,bill_temps.patient_id,bill_temps.created_at').order('bill_temps.created_at desc').paginate(page: params[:page], per_page: 20)
  
  
  if params[:count]
      params[:count]
    else
      params[:count] = 20 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 20
    end

    @per_page = params[:per_page] || BillTemp.per_page || 20

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 20
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)

    if params[:search_value] && params[:search_value].strip != ''
       if params[:search_param] == 'name'
        @bill_temps = BillTemp.name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      end
    elsif params[:search_param] == 'date'
      
    else
    end
  
  
  end

  def bill_open

    @assign_patient = AssignPatient.new
    patient_id=params[:id]

    # @bills = BillHistory.where(patient_id: patient_id)

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id,delete_status: 0 ).order('id desc')

    @bill_total = BillHistory.where(patient_id: patient_id, status: 0,delete_status: 0 )
    @total = @bill_total.sum(:price)

    logger.info "PRICE IS #{@total}"
    # logger.info "record: #{@record.inspect}"

    respond_to do |format|

      format.html
      format.js
    end
  end

  def outstanding
  
    @bill_history_id = params[:id]
    
    @bill_id = BillHistory.find_by(id: @bill_history_id)
    @bill_status = "true"

    #Retrive Service ID from assigned_patients
    @retrive_service_id = BillHistory.where(id: @bill_history_id).select('service_id,patient_id')
    service_id = @retrive_service_id[0].service_id
    patient_id = @retrive_service_id[0].patient_id
    logger.info "SERVICE ID #{@retrive_service_id[0].service_id}"
    logger.info "patient_id ID #{@retrive_service_id[0].service_id}"
    @bill_assign_patient = AssignPatient.where(service_id: service_id, patient_id: patient_id,status: 0)
    logger.info "bill_assign_patient #{@bill_assign_patient.inspect}"
   


    
   if @bill_assign_patient.exists?
  #if (defined?(@assign_patient_id)).nil?
   @assign_patient_id = @bill_assign_patient[0].id
    logger.info "assign_patient_id #{@assign_patient_id}"
    puts AssignPatient.update(@assign_patient_id, :status => 1)
    puts BillHistory.update(@bill_history_id, :status => 1)
    puts BillHistory.update(@bill_history_id, :updated_at => Time.now)
      
  else
  
  puts "---------------ID NOTEXISTING-----------------------------"
  puts BillHistory.update(@bill_history_id, :status => 1)
   puts BillHistory.update(@bill_history_id, :updated_at => Time.now)
  end
    
  end

  def paid
    bill_history_id =params[:id]
    if BillHistory.update(bill_history_id, :status => 0)
      redirect_to bill_temps_path, notice: 'Bills has not been paid'
    end
  end


  def print_all_bill

    patient_id=params[:id]

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id,delete_status: 0 )

    logger.info "BILL HISTOURES #{@bill_histories.inspect}"
    
    @bill_total = BillHistory.where(patient_id: patient_id, status: 1)
    @total = @bill_total.sum(:price)
     logger.info "PRICE IS #{@total}"

    respond_to do |format|

      format.html

      format.pdf do
        pdf = AllBills.new(@bill_histories)
        #pdf.text "HIIIII"
        send_data pdf.render, filename: "Totalbills.pdf",
                                type: "application/pdf",
                                dispostion: "inline"
      end
    end
  end






   def print_bill

    
    bill_id=params[:id]

    # @documents = BillHistory.find_by(id: @bill_history_id)
    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,card_no,dob,bill_histories.price,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status,insurance_cash').where(id: bill_id, delete_status: 0 )

  logger.info "BILL HISTOURES #{@bill_histories.inspect}"


   send_bill_pdf2
  
  
   
  end



  def delete_bill_record

    logger.info "------------------------------------"
 
    created_at=params[:created_at]

    bill_id_obj = BillHistory.find_by(created_at: created_at)
    bill_id = bill_id_obj.id


      # puts "---------PATIENT ID #{patient_id}------------------------------"
       puts "---------bill_id ID #{bill_id}------------------------------"
         puts "---------created_at #{created_at}------------------------------"

      puts BillHistory.update(bill_id, :delete_status => 1)

      redirect_to :back

    #redirect_to lab_docs_path(:id => patient_id), notice: 'Lab request was successfully made.' 
 #    respond_to do |format|
 # # redirect_to :back


 #      format.html
 #      format.js
 #    end

  end


  def view_reciept
     patient_id=params[:id]

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,dob,insurance_cash,other_names,bill_histories.price,card_no,bill_histories.quantity,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id,delete_status: 0 )

    @bill_history = @bill_histories[0].patient_id

    logger.info "BILL HISTOURES #{@bill_history.inspect}"
    logger.info @bill_history
    
    @bill_total = BillHistory.where(patient_id: patient_id, status: 0)
    @total = @bill_total.sum(:price)

     @bill_total_owe = BillHistory.where(patient_id: patient_id, status: 0)
    @bill_total_paid = BillHistory.where(patient_id: patient_id, status: 1)

    @total_owe = @bill_total_owe.sum(:price)
    @total_paid = @bill_total_paid.sum(:price)
    puts "-----TOTAL PAID #{@total_paid}--------------"
     puts "-----TOTAL OWE #{@total_owe}--------------"
     
     send_bill_pdf
  
  end

  def download
    puts "BILL HISTORIESS IN VIEW CONTROLLER"
    puts @bill_histories
    puts @bill_histories[0].surname

    @total_owe
    @total_paid 
    puts "-----TOTAL PAID #{@total_paid}--------------"
     puts "-----TOTAL OWE #{@total_owe}--------------"

    BillPdf.new(@bill_histories, @total_owe, @total_paid )
  end

   def download2
    puts "BILL HISTORIESS IN VIEW CONTROLLER"
    puts @bill_histories
    puts @bill_histories[0].surname
    puts "-------------------"
    BillPdf2.new(@bill_histories)
  end

  def send_bill_pdf
    send_file download.to_pdf, download_attributes
  end

   def send_bill_pdf2
    send_file download2.to_pdf, download_attributes
  end

  def download_attributes
    {
      filename: download.filename,
      type: "application/pdf",
      disposition: "inline"
    }
  end



  # GET /bill_temps/1
  # GET /bill_temps/1.json
  def show
    @bill_histories
  end

  # GET /bill_temps/new
  def new
    @bill_temp = BillTemp.new
  end

  # GET /bill_temps/1/edit
  def edit
  end

  # POST /bill_temps
  # POST /bill_temps.json
  def create
    @bill_temp = BillTemp.new(bill_temp_params)

    respond_to do |format|
      if @bill_temp.save
        format.html { redirect_to @bill_temp, notice: 'Bill temp was successfully created.' }
        format.json { render :show, status: :created, location: @bill_temp }
      else
        format.html { render :new }
        format.json { render json: @bill_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bill_temps/1
  # PATCH/PUT /bill_temps/1.json
  def update
    respond_to do |format|
      if @bill_temp.update(bill_temp_params)
        format.html { redirect_to @bill_temp, notice: 'Bill temp was successfully updated.' }
        format.json { render :show, status: :ok, location: @bill_temp }
      else
        format.html { render :edit }
        format.json { render json: @bill_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bill_temps/1
  # DELETE /bill_temps/1.json
  def destroy
    @bill_temp.destroy
    respond_to do |format|
      format.html { redirect_to bill_temps_url, notice: 'Bill temp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_bill_temp
    @bill_temp = BillTemp.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bill_temp_params
    params.require(:bill_temp).permit(:patient_id, :user_id, :status)
  end
end
