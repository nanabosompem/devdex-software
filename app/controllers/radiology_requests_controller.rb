class RadiologyRequestsController < ApplicationController
  before_action :set_radiology_request, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :radio_docs]
  # require './lib/push/notify.rb' 
  # GET /radiology_requests
  # GET /radiology_requests.json
  def index

    @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = radiology_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,radiology').where(personnel_id: current_user.role_id, delete_status: 0 ).paginate(page: params[:page], per_page: 20)

    @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no').where(personnel_id: current_user.role_id).order('id desc')..paginate(page: params[:page], per_page: 20)
  end

  def other_radio

    @radio_test_result = RadioTestResult.new
    @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = radiology_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,radiology_requests.created_at,radiology_requests.patient_id,personnel_id,radiology_requests.id').order('id desc')

  end
  
  
  def enter_radio_results

   @radio_test_result = RadioTestResult.new
    radio_id=params[:id]
    @radio_id = radio_id

    @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = radiology_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,radiology_requests.created_at,radiology_requests.patient_id,personnel_id,radiology_requests.id').where(id: @radio_id).order('id desc')

    @service_id =  @radiology_requests[0].service_id
    @patient_id = @radiology_requests[0].patient_id
    
    logger.info "LETSSE SEEE PARAMS RADIO_ID #{@radio_id} , service #{@radio_id}, patienrt service #{@patient_id} "

    respond_to do |format|

      format.html
      format.js
    end

  end


   def delete_radio_record

    logger.info "------------------------------------"
 
    created_at=params[:created_at]

    radio_id_obj = RadiologyRequest.find_by(created_at: created_at)
    radio_id = radio_id_obj.id


      # puts "---------PATIENT ID #{patient_id}------------------------------"
       puts "---------radio_id #{radio_id}------------------------------"
         puts "---------created_at #{created_at}------------------------------"

      puts RadiologyRequest.update(radio_id, :delete_status => 1)

      redirect_to :back

    #redirect_to lab_docs_path(:id => patient_id), notice: 'Lab request was successfully made.' 
 #    respond_to do |format|
 # # redirect_to :back


 #      format.html
 #      format.js
 #    end

  end

 

  # GET /radiology_requests/1
  # GET /radiology_requests/1.json
  def show
  end

  # GET /radiology_requests/new
  def new
    @radiology_request = RadiologyRequest.new
  end

  # GET /radiology_requests/1/edit
  def edit
  end

  def radio_docs

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new
    @radiology_request = RadiologyRequest.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"

    if (defined?(patient_id)).nil?

      @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = radiology_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,radiology,card_no,radiology_requests.created_at,radiology_requests.patient_id').where(patient_id: patient_id, delete_status: 0 )

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id').where(patient_id: patient_id )

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(patient_id: patient_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else
      
       @radiology_requests = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN users ON users.id = radiology_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = radiology_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,radiology_requests.created_at,radiology_requests.patient_id,personnel_id,radiology_requests.id').where(patient_id: @patient_id,delete_status: 0).order('id desc')

    
    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def radio_form

   @assign_patient = AssignPatient.new
    @radiology_request = RadiologyRequest.new

    patient_id=params[:id]
    @patient_id = patient_id

     @records = Patient.find_by(id: patient_id)
    @check_insurance = @records.insurance_cash

     logger.info "patient CASH OR INSURANCE: #{@check_insurance}"

    logger.info "Lets see medical id #{@patient_id}"

    @services_inventories = ServicesInventory.order(:service_name).where("service_name like ? AND category_id = 3", "%#{params[:service_id]}" )
    @service_list = @services_inventories.map { |a|[a.service_name+" ",a.id]  }

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    if (defined?(@b_records)).nil?

      @patient_id = @b_records.patient_id
      patient_id = @patient_id

      @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    else
    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_radio

    @assign_patient = AssignPatient.new
    
    patient_id=params[:id]

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    @document1 = RadiologyRequest.joins("INNER JOIN patients ON patients.id = radiology_requests.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = radiology_requests.service_id").select('service_name').where(patient_id: patient_id, delete_status: 0 )

    respond_to do |format|

      format.html
      format.js
    end

  end

  # POST /radiology_requests
  # POST /radiology_requests.json
  def create
     service_id = params[:radiology_request][:service_id]
    patient_id = params[:radiology_request][:patient_id]
    @services_data = ServicesInventory.find_by(id: service_id)
    price = @services_data.price
    logger.info "--------------------------------------------"
    logger.info "-------------service_id#{service_id}-------------------------------"
    logger.info "-------------patient_id#{patient_id}-------------------------------"
    logger.info "-------------price#{price}-------------------------------"
    logger.info "--------------------------------------------"

    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0,:delete_status => 0})
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
    @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0,:delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end
    
    @radiology_request = RadiologyRequest.new(radiology_request_params)


        #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR RADIO")
    #  logger.info push
    #  logger.info "ENDS--------"

    respond_to do |format|
      if @radiology_request.save
        format.html { redirect_to radio_docs_path(:id => patient_id), notice: 'Radiology request was successfully uploaded.' }
        format.json { render :show, status: :created, location: @radiology_request }


         #NOTIFICATION TO RADIOLOGY
        #  notification_for_user = '4d9bb8f2-7025-43de-9cde-f6e1f54ee515'
        #  logger.info "NOTIFCATIONS --------"
        #   push =  Push::Notify.single_notification(notification_for_user,"NEW RADIO REQUEST FROM DOC- CHECK")
        #   logger.info push
        #   logger.info "ENDS--------"
      else
        format.html { render :new }
        format.json { render json: @radiology_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /radiology_requests/1
  # PATCH/PUT /radiology_requests/1.json
  def update
    respond_to do |format|
      if @radiology_request.update(radiology_request_params)
        format.html { redirect_to radiology_requests_path, notice: 'Radiology request was successfully updated.' }
        format.json { render :show, status: :ok, location: @radiology_request }
      else
        format.html { render :edit }
        format.json { render json: @radiology_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /radiology_requests/1
  # DELETE /radiology_requests/1.json
  def destroy
    @radiology_request.destroy
    respond_to do |format|
      format.html { redirect_to radiology_requests_url, notice: 'Radiology request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_radiology_request
    @radiology_request = RadiologyRequest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def radiology_request_params
    params.require(:radiology_request).permit(:delete_status,:basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :radiology, :service_id)
  end
end
