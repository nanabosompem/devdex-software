class TreatmentSheetsController < ApplicationController
  before_action :set_treatment_sheet, only: [:show, :edit, :update, :destroy]

    layout "doc_patients",:only => [ :patient_treatment]


  # GET /treatment_sheets
  # GET /treatment_sheets.json
  def index
     @treatment_sheet = TreatmentSheet.new

     patient_id =params[:id]
    @patient_id = patient_id



    logger.info "PATEITN ID #{@patient_id}"

    #@treatment_sheets = TreatmentSheet.where(patient_id: patient_id).order('id desc')

    @treatment_sheets = TreatmentSheet.joins("INNER JOIN patients ON patients.id = treatment_sheets.patient_id
                  INNER JOIN users ON users.id  = treatment_sheets.user_id").select('surname,other_names,patients.mobile_number,drugs,time,fullname,occupation,address,dob,patients.user_id,treatment_sheets.patient_id,treatment_sheets.id,treatment_sheets.status,card_no,treatment_sheets.created_at').where(patient_id: patient_id).order('id desc')

  end

  def patient_treatment
   @treatment_sheet = TreatmentSheet.new

     patient_id =params[:id]
    @patient_id = patient_id

     @patient_records = Patient.find_by(id: patient_id)

    logger.info "PATEITN ID #{@patient_id}"

    @treatment_sheets = TreatmentSheet.joins("INNER JOIN patients ON patients.id = treatment_sheets.patient_id
                  INNER JOIN users ON users.id  = treatment_sheets.user_id").select('surname,other_names,patients.mobile_number,drugs,time,fullname,occupation,address,dob,patients.user_id,treatment_sheets.patient_id,treatment_sheets.id,treatment_sheets.status,card_no,treatment_sheets.created_at').where(patient_id: patient_id).order('id desc')


  end

  def new_treatment

    patient_id =params[:id]
    @patient_id = patient_id
    logger.info "PATEITN ID #{@patient_id}"
    @treatment_sheet = TreatmentSheet.new

  end

  # GET /treatment_sheets/1
  # GET /treatment_sheets/1.json
  def show
  end

  # GET /treatment_sheets/new
  def new

    
    @treatment_sheet = TreatmentSheet.new

  end

  # GET /treatment_sheets/1/edit
  def edit
  end

  # POST /treatment_sheets
  # POST /treatment_sheets.json
  def create
    @treatment_sheet = TreatmentSheet.new(treatment_sheet_params)

    respond_to do |format|
      if @treatment_sheet.save
        format.html { redirect_to :back, notice: 'Treatment sheet was successfully created.' }
        format.json { render :show, status: :created, location: @treatment_sheet }
      else
        format.html { render :new }
        format.json { render json: @treatment_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /treatment_sheets/1
  # PATCH/PUT /treatment_sheets/1.json
  def update
    respond_to do |format|
      if @treatment_sheet.update(treatment_sheet_params)
        format.html { redirect_to @treatment_sheet, notice: 'Treatment sheet was successfully updated.' }
        format.json { render :show, status: :ok, location: @treatment_sheet }
      else
        format.html { render :edit }
        format.json { render json: @treatment_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /treatment_sheets/1
  # DELETE /treatment_sheets/1.json
  def destroy
    @treatment_sheet.destroy
    respond_to do |format|
      format.html { redirect_to treatment_sheets_url, notice: 'Treatment sheet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_treatment_sheet
      @treatment_sheet = TreatmentSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def treatment_sheet_params
      params.require(:treatment_sheet).permit(:drugs, :user_id, :time, :patient_id, :status)
    end
end
