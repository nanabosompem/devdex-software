class HomeController < ApplicationController
  def index

  	@patients = Patient.all

  	# @users = User.all(:conditions => ["created_at >= ?", Date.today.at_beginning_of_month])
  	@patient_stats = Patient.all.count

  	@lastday = Patient.where( 'created_at >= :one_days_ago', :one_days_ago => 1.days.ago,).count


  @date_start = DateTime.now
  @date_end = @date_start - 24.hour
  @dailyDoctor = AssignPatient.where(:created_at => @date_end..@date_start , :role_id => 3).count
  @dailyappointments = Appointment.where(:created_at => @date_end..@date_start ).count

  	#@dailyDoctor = AssignPatient.where( 'created_at >= :one_days_ago, role_id' ,:one_days_ago => 1.days.ago , :role_id => 3).count
  #	@dailyDoctor = AssignPatient.where(role_id: 3, created_at >=  1.days.ago ).count

  	logger.info "SEE THE STATS #{@patient_stats}"
  	logger.info "SEE THE STATS lastday #{@lastday}"
  	logger.info "SEE THE STATS dailyDoctor #{@dailyDoctor}"
  	logger.info "SEE THE STATS dailyappointments #{@dailyappointments}"
  end
end
