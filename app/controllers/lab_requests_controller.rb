class LabRequestsController < ApplicationController
  before_action :set_lab_request, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :lab_docs]
  # require './lib/push/notify.rb' 

  autocomplete :services_inventories, :service_name ,:full => true
  # GET /lab_requests
  # GET /lab_requests.json
  def index

    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,personnel_id').where(personnel_id: current_user.role_id ,delete_status: 0 ).paginate(page: params[:page], per_page: 20)
    @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no').where(personnel_id: current_user.role_id).order('id desc').paginate(page: params[:page], per_page: 20)

  end

  def other_lab
    @test_result = TestResult.new

    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id ").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc').paginate(:page => params[:page], :per_page => 10).order('created_at desc') 
       if @lab_requests.exists?
    @service_id =  @lab_requests[0].service_id
    logger.info "SERVICE ID #{@service_id}"
  else
  end


   if params[:count]
      params[:count]
    else
      params[:count] = 20 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 20
    end

    @per_page = params[:per_page] || LabRequest.per_page || 20

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 20
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)

    if params[:search_value] && params[:search_value].strip != ''
      if params[:search_param] == 'card_no'
        @lab_requests = LabRequest.patient_no_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      elsif params[:search_param] == 'mobile_number'
        @lab_requests = LabRequest.mobile_number_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      elsif params[:search_param] == 'name'
        @lab_requests = LabRequest.name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
       elsif params[:search_param] == 'f_name'
        @patients = LabRequest.f_name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      end
    elsif params[:search_param] == 'date'
      start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
      ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"

      @lab_requests = LabRequest.date_search(start,ended).paginate(page: params[:page], per_page: params[:count]).order('created_at asc')

    else
    end


  end

  def lab_history

    @assign_patient = AssignPatient.new

    patient_id =params[:id]

    @lab_history = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_type').where(patient_id: patient_id,delete_status: 0 )

    respond_to do |format|

      format.html
      format.js
    end

  end

  def enter_results

    @test_result = TestResult.new
    lab_id=params[:id]
    @lab_id = lab_id

    @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(id: @lab_id, delete_status: 0).order('id desc')

    @service_id =  @lab_requests[0].service_id
    @patient_id = @lab_requests[0].patient_id

    respond_to do |format|

      format.html
      format.js
    end

  end

  def lab_medical_records
    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    
    retrieve_dob = @patients1.dob
    now = Time.now.utc.to_date

    logger.info "now IS #{now.year} dob #{retrieve_dob.year}"
    @age = now.year - retrieve_dob.year
    logger.info "AGE IS #{@age}"
     @b_records = BasicMedRecord.where(patient_id: patient_id).order('id desc')
     
    @blood_pressure = @b_records[0].blood_pressure
    @pulse = @b_records[0].pulse
    @respiratory = @b_records[0].respiratory_rate
    @temperature =@b_records[0].temp
    @patient_id = @b_records[0].patient_id
    @height  = @b_records[0].height
    @weight  = @b_records[0].weight
    @saturation  = @b_records[0].saturation


   
    
     @clinic_records = ClinicDocument.where(patient_id: patient_id).order('id desc')

      if @clinic_records.exists?
     @complaints = @clinic_records[0].complaints
     @med_history = @clinic_records[0].med_history
     @social_history = @clinic_records[0].social_history
     @allergies = @clinic_records[0].allergies
     @final_diagnosis = @clinic_records[0].final_diagnosis
     @time = @clinic_records[0].created_at.strftime("%B %d, %Y %H:%M")

     logger.info "Lets see clinic_records #{@clinic_records.inspect}"
     logger.info "Lets see complaints #{@complaints}"

     else
     end




 

    respond_to do |format|

      format.html
      format.js
    end
  end
  
  def lab_docs

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new
    @lab_request = LabRequest.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

     @b_records = BasicMedRecord.where(patient_id: patient_id).order('id desc')
     
    @blood_pressure = @b_records[0].blood_pressure
    @pulse = @b_records[0].pulse
    @respiratory = @b_records[0].respiratory_rate
    @temperature =@b_records[0].temp
    @patient_id = @b_records[0].patient_id
    @height  = @b_records[0].height
    @weight  = @b_records[0].weight
    @saturation  = @b_records[0].saturation


    #@clinic_records = ClinicDocument.find_by(patient_id: patient_id).order('id desc')
    
    @clinic_records = ClinicDocument.where(patient_id: patient_id).order('id desc')
     if @clinic_records.exists?
     @complaints = @clinic_records[0].complaints
     @med_history = @clinic_records[0].med_history
     @social_history = @clinic_records[0].social_history
     @allergies = @clinic_records[0].allergies
      # logger.info "Lets see clinic_records #{@clinic_records.inspect}"
     # logger.info "Lets see complaints #{@complaints}"
     else
     end

    
     
     
     

    @vital_signs = "Respiratory Rate: " + @b_records[0].respiratory_rate + " " + "Pulse: " +  @b_records[0].pulse + " " + "Blood Pressure: " +  @b_records[0].blood_pressure + " " + "Temperature: " +  @b_records[0].temp + " " + "Oxygen saturation: " +  @b_records[0].saturation.to_s + " " + "Weight: " +  @b_records[0].weight.to_s + " " + "Height: " +  @b_records[0].height.to_s

    logger.info "Vital signs: #{@vital_signs}"

    if (defined?(patient_id)).nil?

      @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc').paginate(:page => params[:page], :per_page => 5).order('created_at desc') 

      @b_records = BasicMedRecord.find_by(id: patient_id)
      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{basic_med_id}"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id').where(patient_id: patient_id )

      logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"

      @document1 = ClinicDocument.find_by(id: basic_med_id)
      logger.info "document1: #{@document1}"
      logger.info "complaints: #{@document1.complaints}"

    else

      @lab_requests = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(patient_id: @patient_id,  delete_status: 0 ).order('id desc').paginate(:page => params[:page], :per_page => 5).order('created_at desc') 

    end

    respond_to do |format|

      format.html
      format.js
    end

  end


   def print_labresults

    patient_id =params[:id]

     # @lab_history = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
     #                INNER JOIN users ON users.id = lab_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = lab_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_type').where(patient_id: patient_id)
   @lab_history = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id INNER JOIN users ON users.id = lab_requests.user_id
                    ").select('surname,other_names,patients.mobile_number,card_no,occupation,address,dob,lab_requests.created_at,users.fullname,lab_requests.patient_id').where(patient_id: patient_id,  delete_status: 0 )

    respond_to do |format|

      format.html

      format.pdf do
        pdf = PrintLabResultsPdf.new(@lab_history)
        send_data pdf.render, filename: "labresults.pdf",
                                type: "application/pdf",
                                dispostion: "inline"
      end
    end
  end

  # GET /lab_requests/1
  # GET /lab_requests/1.json
  def show
  end

  # GET /lab_requests/new
  def new
    @lab_request = LabRequest.new
  end

  # GET /lab_requests/1/edit
  def edit
  end



 

  

  def lab_form
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new

    patient_id=params[:id]
    @patient_id = patient_id


     @records = Patient.find_by(id: patient_id)
    @check_insurance = @records.insurance_cash
    
    logger.info "patient CASH OR INSURANCE: #{@check_insurance}"


    logger.info "Lets see medical id #{@patient_id}"

    @services_inventories = ServicesInventory.order(:service_name).where("service_name like ? AND category_id = 1", "%#{params[:service_id]}" )
    @service_list = @services_inventories.map { |a|[a.service_name + " ",a.id]  }

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    if (defined?(@b_records)).nil?

      @patient_id = @b_records.patient_id
      patient_id = @patient_id

      @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    else
    end

    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_lab

    logger.info "------------------------------------"
    puts "---------------------------------------"

    @assign_patient = AssignPatient.new
    
    patient_id=params[:id]

    @b_records = BasicMedRecord.find_by(patient_id: patient_id)

    @document1 = LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id").select('service_name').where(patient_id: patient_id, delete_status: 0 )

    logger.info "CHECKINDG LAB DETAILS HERE #{ @document1}"
    respond_to do |format|

      format.html
      format.js
    end

  end





   def delete_lab_record

    logger.info "------------------------------------"
 
    created_at=params[:created_at]

    lab_id_obj = LabRequest.find_by(created_at: created_at)
    lab_id = lab_id_obj.id


      # puts "---------PATIENT ID #{patient_id}------------------------------"
       puts "---------Lab ID #{lab_id}------------------------------"
         puts "---------created_at #{created_at}------------------------------"

      puts LabRequest.update(lab_id, :delete_status => 1)

      redirect_to :back

    #redirect_to lab_docs_path(:id => patient_id), notice: 'Lab request was successfully made.' 
 #    respond_to do |format|
 # # redirect_to :back


 #      format.html
 #      format.js
 #    end

  end
  
  
   # POST /lab_requests
  # POST /lab_requests.json
  def create

    service_id = params[:lab_request][:service_id]
    patient_id = params[:lab_request][:patient_id]
    logger.info "-------------service_id #{service_id}-------------------------------"
     logger.info "-------------patient_id#{patient_id}-------------------------------"

  if service_id.blank?
    # present?
    # blank?
     logger.info "-------------No service ID here-------------------------------"
     respond_to do |format|
        # redirect_to lab_docs_path(:id => patient_id), notice: "Kindly select a lab type "
         format.html { redirect_to lab_docs_path(:id => patient_id), notice: 'Sorry! You did not select any lab type. Kindly select a lab type' }
          #redirect_to lab_form_path(:id => @patient_id), notice: "Kindly select a lab type "  
        end
else
  @services_data = ServicesInventory.find_by(id: service_id)
    price = @services_data.price
    logger.info "--------------------------------------------"
    logger.info "-------------service_id#{service_id}-------------------------------"
    logger.info "-------------patient_id#{patient_id}-------------------------------"
    logger.info "-------------price#{price}-------------------------------"
    logger.info "--------------------------------------------"

    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0 , :delete_status => 0 })
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
    @bills.save

    else
      @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0, :delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end

    @lab_request = LabRequest.new(lab_request_params)

    #assign_patients_path

  


    respond_to do |format|
      if @lab_request.save
        format.html { redirect_to lab_docs_path(:id => patient_id), notice: 'Lab request was successfully made.' }
        format.json { render :show, status: :created, location: @lab_request }


         #NOTIFICATION TO LAB TECHNICIAN
        #  notification_for_user = '00670073-92c7-45ce-b89a-76ef9d83f963'
        #  logger.info "NOTIFCATIONS --------"
        #   push =  Push::Notify.single_notification(notification_for_user,"NEW LAB REQUEST FROM DOC - CHECK")
        #   logger.info push
        #   logger.info "ENDS--------"

            #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR LAB REQUESTS")
    #  logger.info push
    #  logger.info "ENDS--------"
      else
        format.html { render :new }
        format.json { render json: @lab_request.errors, status: :unprocessable_entity }
      end
    end
  end
  end

  # PATCH/PUT /lab_requests/1
  # PATCH/PUT /lab_requests/1.json
  def update
    respond_to do |format|
      if @lab_request.update(lab_request_params)
        format.html { redirect_to @lab_request, notice: 'Lab request was successfully updated.' }
        format.json { render :show, status: :ok, location: @lab_request }
      else
        format.html { render :edit }
        format.json { render json: @lab_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lab_requests/1
  # DELETE /lab_requests/1.json
  def destroy
    @lab_request.destroy
    respond_to do |format|
      format.html { redirect_to lab_requests_url, notice: 'Lab request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_lab_request
    @lab_request = LabRequest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def lab_request_params
    params.require(:lab_request).permit(:delete_status,:basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :lab_type,:service_id)
  end
end
