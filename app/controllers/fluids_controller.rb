class FluidsController < ApplicationController
  before_action :set_fluid, only: [:show, :edit, :update, :destroy]

   layout "doc_patients",:only => [ :fluids_intake]

  # GET /fluids
  # GET /fluids.json
  def index
     @fluid = Fluid.new
    @fluids = Fluid.all
  end


  def fluids_intake
    @fluid = Fluid.new
    patient_id =params[:id]
    @patient_id = patient_id

     @patient_records = Patient.find_by(id: patient_id)
     logger.info "PATEITN ID #{@patient_id}"


      @fluids = Fluid.joins("INNER JOIN patients ON patients.id = fluids.patient_id
                  INNER JOIN users ON users.id  = fluids.user_id").select('surname,other_names,patients.mobile_number,time,fullname,occupation,address,dob,patients.user_id,fluids.patient_id,fluids.id,fluids.status,card_no,fluids.created_at,orals,orals_amount,iv_fluids,fluids_amount,urine_amount,emesis_amount,drainage_amount').where(patient_id: patient_id).order('id desc')

  end


  def new_fluids_intake
     patient_id =params[:id]
    @patient_id = patient_id
    logger.info "PATEITN ID #{@patient_id}"
     @fluid = Fluid.new
  end

  # GET /fluids/1
  # GET /fluids/1.json
  def show
  end

  # GET /fluids/new
  def new
    @fluid = Fluid.new
  end

  # GET /fluids/1/edit
  def edit
  end

  # POST /fluids
  # POST /fluids.json
  def create
    @fluid = Fluid.new(fluid_params)

    respond_to do |format|
      if @fluid.save
        format.html { redirect_to :back, notice: 'Fluid was successfully created.' }
        format.json { render :show, status: :created, location: @fluid }
      else
        format.html { render :new }
        format.json { render json: @fluid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fluids/1
  # PATCH/PUT /fluids/1.json
  def update
    respond_to do |format|
      if @fluid.update(fluid_params)
        format.html { redirect_to @fluid, notice: 'Fluid was successfully updated.' }
        format.json { render :show, status: :ok, location: @fluid }
      else
        format.html { render :edit }
        format.json { render json: @fluid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fluids/1
  # DELETE /fluids/1.json
  def destroy
    @fluid.destroy
    respond_to do |format|
      format.html { redirect_to fluids_url, notice: 'Fluid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fluid
      @fluid = Fluid.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fluid_params
      params.require(:fluid).permit(:orals, :orals_amount, :iv_fluids, :fluids_amount, :urine_amount, :emesis_amount, :drainage_amount, :user_id, :time, :patient_id, :status)
    end
end
