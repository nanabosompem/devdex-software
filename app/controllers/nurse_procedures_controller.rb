class NurseProceduresController < ApplicationController
  before_action :set_nurse_procedure, only: [:show, :edit, :update, :destroy]

  # GET /nurse_procedures
  # GET /nurse_procedures.json
  def index
    @nurse_procedures = NurseProcedure.joins("INNER JOIN patients ON patients.id = nurse_procedures.patient_id
                    INNER JOIN users ON users.id = nurse_procedures.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,nurse_procedures.id,nurse_procedures.created_at').order('id desc')
   
  end



  def view_procedure_record

     created_at=params[:id]

     @documents = NurseProcedure.joins("INNER JOIN services_inventories ON services_inventories.id = nurse_procedures.service_id
                    ").select('complaints,service_name,price,nurse_procedures.id,nurse_procedures.created_at').where(created_at: created_at).order('id desc')
   

    logger.info "documents: #{@documents.inspect}"

    @proced = "Service Name" + @documents[0].service_name + " " + "Complaints " +  @documents[0].complaints

    logger.info "proced signs: #{@proced}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  # GET /nurse_procedures/1
  # GET /nurse_procedures/1.json
  def show
  end

  # GET /nurse_procedures/new
  def new
    @nurse_procedure = NurseProcedure.new
  end

  # GET /nurse_procedures/1/edit
  def edit
  end

  # POST /nurse_procedures
  # POST /nurse_procedures.json
  def create
  service_id  = params[:service_id]
  logger.info "---SERRVICE ID ARRAY IS #{service_id}"

  
 if service_id.blank?
     logger.info "-------------No service ID here-------------------------------"
     respond_to do |format|
         format.html { redirect_to assign_patients_path, notice: 'Sorry! You did not select any procedure. Kindly select a procedure' }
        end
else

   service_id.each do |service|

   logger.info "---SERRVICE ID IS #{service}"
      
   complaints  = params[:nurse_procedure][:complaints]
   user_id  = params[:nurse_procedure][:user_id]
  patient_id  = params[:nurse_procedure][:patient_id]
   status  = params[:nurse_procedure][:status]

  @services_data = ServicesInventory.find_by(id: service)
    price = @services_data.price

    logger.info "-------------price#{price}-------------------------------"
    logger.info "--------------------------------------------"

    if BillTemp.where(patient_id: patient_id).exists?
      @id = BillTemp.find_by(patient_id: patient_id)
      @bills = BillHistory.new({:service_id => service,:patient_id => patient_id, :price => price ,:status=> 0 , :delete_status => 0 })
      @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
    @bills.save

    else
      @bills = BillHistory.new({:service_id => service,:patient_id => patient_id, :price => price ,:status=> 0, :delete_status => 0})
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills.save
    @bills_temp.save
    end

   @info = NurseProcedure.new({:service_id => service,:complaints => complaints,:user_id => user_id,:patient_id => patient_id,:status => status})
   @info.save 
      end

      
      respond_to do |format|
      
          format.html { redirect_to nurse_procedures_path, notice: 'Nurse procedure was successfully created.' }
        format.js
    end
  end
   

  end

  # PATCH/PUT /nurse_procedures/1
  # PATCH/PUT /nurse_procedures/1.json
  def update
    respond_to do |format|
      if @nurse_procedure.update(nurse_procedure_params)
        format.html { redirect_to @nurse_procedure, notice: 'Nurse procedure was successfully updated.' }
        format.json { render :show, status: :ok, location: @nurse_procedure }
      else
        format.html { render :edit }
        format.json { render json: @nurse_procedure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nurse_procedures/1
  # DELETE /nurse_procedures/1.json
  def destroy
    @nurse_procedure.destroy
    respond_to do |format|
      format.html { redirect_to nurse_procedures_url, notice: 'Nurse procedure was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nurse_procedure
      @nurse_procedure = NurseProcedure.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nurse_procedure_params
      params.require(:nurse_procedure).permit(:user_id, :service_id, :patient_id, :status, :complaints)
    end
end
