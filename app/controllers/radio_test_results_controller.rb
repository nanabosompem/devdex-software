class RadioTestResultsController < ApplicationController
  before_action :set_radio_test_result, only: [:show, :edit, :update, :destroy]
layout "doc_patients",:only => [ :indi_radio_result]
  # GET /radio_test_results
  # GET /radio_test_results.json
  def index
    @radio_test_results = RadioTestResult.all
    
    @radio_test_results = RadioTestResult.joins("INNER JOIN patients ON patients.id = radio_test_results.patient_id
                    INNER JOIN users ON users.id = radio_test_results.user_id INNER JOIN radiology_requests ON radiology_requests.id = radio_test_results.radio_id INNER JOIN services_inventories ON services_inventories.id = radio_test_results.service_id").select('radio_test_results.document_file_name,service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,results,radio_test_results.patient_id,radio_test_results.created_at,radio_test_results.radio_id,radio_test_results.id').order('id desc').paginate(page: params[:page], per_page: 20)

  end
  
  def indi_radio_result
     patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    @radio_test_results = RadioTestResult.joins("INNER JOIN patients ON patients.id = radio_test_results.patient_id
                    INNER JOIN users ON users.id = radio_test_results.user_id INNER JOIN radiology_requests ON radiology_requests.id = radio_test_results.radio_id INNER JOIN services_inventories ON services_inventories.id = radio_test_results.service_id").select('radio_test_results.document_file_name,service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,results,radio_test_results.patient_id,radio_test_results.created_at,radio_test_results.radio_id,radio_test_results.id').where(patient_id: patient_id ).order('id desc')

  end

  # GET /radio_test_results/1
  # GET /radio_test_results/1.json
  def show
  end

  # GET /radio_test_results/new
  def new
    @radio_test_result = RadioTestResult.new
  end

  # GET /radio_test_results/1/edit
  def edit
  end

  # POST /radio_test_results
  # POST /radio_test_results.json
  def create
    @radio_test_result = RadioTestResult.new(radio_test_result_params)
    @doc1 = params[:radio_test_result][:document]
    
    
      logger.info "Lets see params2 #{@doc1}"
     if @doc1.nil?
        redirect_to other_radio_path, notice: "Kindly select A file to upload"
      else   

    respond_to do |format|
      if @radio_test_result.save
        format.html { redirect_to radio_test_results_path, notice: 'Radio test result was successfully created.' }
        format.json { render :show, status: :created, location: @radio_test_result }
      else
        format.html { render :new }
        format.json { render json: @radio_test_result.errors, status: :unprocessable_entity }
      end
    end
   end
  end

  # PATCH/PUT /radio_test_results/1
  # PATCH/PUT /radio_test_results/1.json
  def update
    respond_to do |format|
      if @radio_test_result.update(radio_test_result_params)
        format.html { redirect_to @radio_test_result, notice: 'Radio test result was successfully updated.' }
        format.json { render :show, status: :ok, location: @radio_test_result }
      else
        format.html { render :edit }
        format.json { render json: @radio_test_result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /radio_test_results/1
  # DELETE /radio_test_results/1.json
  def destroy
    @radio_test_result.destroy
    respond_to do |format|
      format.html { redirect_to radio_test_results_url, notice: 'Radio test result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_radio_test_result
      @radio_test_result = RadioTestResult.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def radio_test_result_params
      
       params.require(:radio_test_result).permit(:patient_id, :user_id, :status, :results, :radio_id, :service_id, :document)
    end
end
