class OrderRequestsController < ApplicationController
  before_action :set_order_request, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :order_records]
  # require './lib/push/notify.rb' 
  # GET /order_requests
  # GET /order_requests.json
  def index
    @order_request = OrderRequest.new
    @order_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN users ON users.id = order_requests.user_id  INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('order_requests.drug_quantity,order_requests.dosage,order_requests.id,services_inventories.service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id,order_requests.status').paginate(page: params[:page], per_page: 20)
  end

  def order_records

    @assign_patient = AssignPatient.new
    @order_request = OrderRequest.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)

    @order_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN users ON users.id = order_requests.user_id INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('order_requests.drug_quantity,order_requests.dosage,order_requests.status,order_requests.id,services_inventories.service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id').where(patient_id: patient_id, delete_status: 0 ).order('order_requests.id desc').paginate(page: params[:page], per_page: 5 )


    respond_to do |format|

      format.html
      format.js
    end
  end



   def delete_order_record

    logger.info "------------------------------------"
 
    created_at=params[:created_at]

    order_id_obj = OrderRequest.find_by(created_at: created_at)
    order_id = order_id_obj.id


      # puts "---------PATIENT ID #{patient_id}------------------------------"
       puts "---------order_id #{order_id}------------------------------"
         puts "---------created_at #{created_at}------------------------------"

      puts OrderRequest.update(order_id, :delete_status => 1)

      redirect_to :back

  end

  def make_order

  
    @order_request = OrderRequest.new
    @lab_request = LabRequest.new

    patient_id=params[:id]
    @patient_id = patient_id

    logger.info "Lets see medical id #{@patient_id}"

        
    @services_inventories = ServicesInventory.order(:service_name).where("service_name like ? AND (category_id != 1 AND category_id != 3)", "%#{params[:service_id]}" )
    @service_list = @services_inventories.map { |a|[a.service_name+" ",a.id]  }
  
  
    respond_to do |format|

      format.html
      format.js
    end
  end

  def view_order

    @assign_patient = AssignPatient.new
    patient_id=params[:id]

    # @document1 = OrderRequest.find_by(patient_id: patient_id)

    @document1 = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN order_masters ON order_masters.id = order_requests.order_master_id").select('order_name').where(patient_id: patient_id )

    respond_to do |format|

      format.html
      format.js
    end
  end

  def close_order

    @order_request = OrderRequest.new

    @created_at = params[:id]
    logger.info "created_at: #{@created_at}"

    @order_id = OrderRequest.find_by(created_at: @created_at)
    @order_status = "true"
    logger.info "Order_status: #{@order_status}"
    logger.info "Order_id: #{@order_id.id}"
    logger.info "Order_status: #{@order_id.status}"
    logger.info "Created at: #{@order_id.created_at}"
    logger.info "Patient ID: #{@order_id.patient_id}"
    logger.info "Service ID: #{@order_id.service_id}"

   OrderRequest.update(@order_id.id, :status => 1)

  end

  # GET /order_requests/1
  # GET /order_requests/1.json
  def show
  end

  # GET /order_requests/new
  def new
    @order_request = OrderRequest.new
  end

  # GET /order_requests/1/edit
  def edit
  end

  # POST /order_requests
  # POST /order_requests.json
  def create
    patient_id = params[:order_request][:patient_id]
    service_id = params[:order_request][:service_id]
    @quantity = params[:order_request][:drug_quantity]
    dosage = params[:order_request][:dosage]

     if service_id.blank?
   
     logger.info "-------------No service ID here-------------------------------"
     respond_to do |format|
                 format.html { redirect_to order_records_path(:id => patient_id), notice: 'Sorry! You did not select any order. Kindly select a lab type' }
          #redirect_to lab_form_path(:id => @patient_id), notice: "Kindly select a lab type "  
        end
else

    logger.info "------------------SERVICE ID #{service_id}-------------------------"
    @category = ServicesInventory.where(id: service_id)
    @category_id = @category[0].category_id

    
    @services_data = ServicesInventory.find_by(id: service_id)
    @price = "%.2f" % ( @services_data.price.to_f * @quantity.to_f )

 

     logger.info "-------------TOTAL COST OF DRUG #{@price}-------------------------------"

    if @category_id == 2

      if MedicationTemp.where(patient_id: patient_id).exists?
        @id = MedicationTemp.find_by(patient_id: patient_id)
        @medication_temp = MedicationTemp.update(@id.id, :created_at => Time.now )
      else
        @medication_temp = MedicationTemp.new({:patient_id => patient_id, :service_id => service_id,:status=> 0})
        @medication_temp.save
      end
      
      if BillTemp.where(patient_id: patient_id).exists?
        @id = BillTemp.find_by(patient_id: patient_id)
       @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => @price ,:status=> 0,:delete_status => 0})
       @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
       @bills.save
      
      else
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => @price ,:status=> 0,:delete_status => 0})
        @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
        @bills.save
        @bills_temp.save
      end
          
    else
       if BillTemp.where(patient_id: patient_id).exists?
        @id = BillTemp.find_by(patient_id: patient_id)
       @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => @price ,:status=> 0,:delete_status => 0})
       @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
       @bills.save
      
      else
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => @price ,:status=> 0,:delete_status => 0})
        @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
        @bills.save
        @bills_temp.save
      end
    end
    
    @order_request = OrderRequest.new(order_request_params)


    respond_to do |format|
   if @order_request.save
      format.html { redirect_to order_records_path(:id => patient_id), notice: 'Order request was successfully created.' }
      format.json { render :show, status: :created, location: @order_request }


       #NOTIFICATION TO PHARMACIST
      #  notification_for_user = '795d5831-6b23-43c1-8bec-d54e63f11271'
      #  logger.info "NOTIFCATIONS --------"
      #   push =  Push::Notify.single_notification(notification_for_user,"NEW MEDICATION REQUEST FROM DOC - CHECK")
      #   logger.info push
      #   logger.info "ENDS--------"


        #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR MEDICATIONS")
    #  logger.info push
    #  logger.info "ENDS--------"
    else
    format.html { render :new }
    format.json { render json: @order_request.errors, status: :unprocessable_entity }
    end
    end

  end
  end

  # PATCH/PUT /order_requests/1
  # PATCH/PUT /order_requests/1.json
  def update
    respond_to do |format|
      if @order_request.update(order_request_params)
        format.html { redirect_to @order_request, notice: 'Order request was successfully updated.' }
        format.json { render :show, status: :ok, location: @order_request }
      else
        format.html { render :edit }
        format.json { render json: @order_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_requests/1
  # DELETE /order_requests/1.json
  def destroy
    @order_request.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Order was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order_request
    @order_request = OrderRequest.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_request_params
    params.require(:order_request).permit(:delete_status,:dosage,:drug_quantity,:patient_id, :user_id, :personnel_id, :status, :order_master_id, :service_id)
  end
end
