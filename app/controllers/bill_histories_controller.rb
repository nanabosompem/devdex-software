class BillHistoriesController < ApplicationController
  before_action :set_bill_history, only: [:show, :edit, :update, :destroy]
  layout "doc_patients",:only => [ :patient_bill]
  # GET /bill_histories
  # GET /bill_histories.json
  def index

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('surname,other_names,bill_histories.price,service_name,bill_histories.patient_id,bill_histories.created_at').where(delete_status: 0).paginate(page: params[:page], per_page: 20)
  end

  def patient_bill

    @bill_history = BillHistory.new
    logger.info 'Ruuinin new'
    patient_id =params[:id]
    @patient_id = patient_id
    logger.info 'Patient ID is #{@patient_id}'
    @patient_records = Patient.find_by(id: patient_id)

    @bill_histories = BillHistory.joins("INNER JOIN patients ON patients.id = bill_histories.patient_id
                    INNER JOIN services_inventories ON services_inventories.id = bill_histories.service_id").select('bill_histories.id,surname,other_names,bill_histories.price,service_name,bill_histories.patient_id,bill_histories.created_at,bill_histories.status').where(patient_id: patient_id, delete_status: 0 ).order('id desc')
  end

  # GET /bill_histories/1
  # GET /bill_histories/1.json
  def show
  end

  # GET /bill_histories/new
  def new
    @bill_history = BillHistory.new
  end

  # GET /bill_histories/1/edit
  def edit
  end

  # POST /bill_histories
  # POST /bill_histories.json
  def create
    @bill_history = BillHistory.new(bill_history_params)

    respond_to do |format|
      if @bill_history.save
        format.html { redirect_to bill_histories_path, notice: 'Bill history was successfully created.' }
        format.json { render :show, status: :created, location: @bill_history }
      else
        format.html { render :new }
        format.json { render json: @bill_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bill_histories/1
  # PATCH/PUT /bill_histories/1.json
  def update
    respond_to do |format|
      if @bill_history.update(bill_history_params)
        format.html { redirect_to bill_histories_path, notice: 'Bill history was successfully updated.' }
        format.json { render :show, status: :ok, location: @bill_history }
      else
        format.html { render :edit }
        format.json { render json: @bill_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bill_histories/1
  # DELETE /bill_histories/1.json
  def destroy
    @bill_history.destroy
    respond_to do |format|
      format.html { redirect_to bill_histories_url, notice: 'Bill history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_bill_history
    @bill_history = BillHistory.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bill_history_params
    params.require(:bill_history).permit(:delete_status,:service_id, :patient_id, :price, :status, :quantity, :unit_price)
  end
end
