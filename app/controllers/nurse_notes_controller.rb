class NurseNotesController < ApplicationController
  before_action :set_nurse_note, only: [:show, :edit, :update, :destroy]

  # GET /nurse_notes
  # GET /nurse_notes.json
  def index
    @nurse_notes = NurseNote.all
  end

  # GET /nurse_notes/1
  # GET /nurse_notes/1.json
  def show
  end

  # GET /nurse_notes/new
  def new
    @nurse_note = NurseNote.new
  end

  # GET /nurse_notes/1/edit
  def edit
  end

  # POST /nurse_notes
  # POST /nurse_notes.json
  def create
    @nurse_note = NurseNote.new(nurse_note_params)

    respond_to do |format|
      if @nurse_note.save
        format.html { redirect_to :back, notice: 'Nurse note was successfully created.' }
        format.json { render :show, status: :created, location: @nurse_note }
      else
        format.html { render :new }
        format.json { render json: @nurse_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nurse_notes/1
  # PATCH/PUT /nurse_notes/1.json
  def update
    respond_to do |format|
      if @nurse_note.update(nurse_note_params)
        format.html { redirect_to @nurse_note, notice: 'Nurse note was successfully updated.' }
        format.json { render :show, status: :ok, location: @nurse_note }
      else
        format.html { render :edit }
        format.json { render json: @nurse_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nurse_notes/1
  # DELETE /nurse_notes/1.json
  def destroy
    @nurse_note.destroy
    respond_to do |format|
      format.html { redirect_to nurse_notes_url, notice: 'Nurse note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nurse_note
      @nurse_note = NurseNote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nurse_note_params
      params.require(:nurse_note).permit(:notes, :user_id, :time, :patient_id, :status)
    end
end
