class LabRequest < ActiveRecord::Base

 has_attached_file :document
validates_attachment :document, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }
  
  # validates :results,:code, presence: true
  # validates :document,:code, presence: true
  
  #validates :document, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

def self.per_page
    20
  end
  
def self.patient_no_search(card_no)
  card_no = "%#{card_no}%"
  where('card_no LIKE ?', card_no) 

  LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id ").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc').where('card_no LIKE ?', card_no) 
end


 def self.date_search(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
      end
 end


def self.mobile_number_search(mobile_number)
  mobile_number = "%#{mobile_number}%"
  # where("customer_name LIKE ?",mobile_number)


  LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id ").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc').where('mobile_number LIKE ?', mobile_number)
end


def self.name_search(name)
  name = "%#{name}%"
 

  LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id ").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc'). where('surname LIKE ?', name)
end


def self.f_name_search(f_name)
  f_name = "%#{f_name}%"
  # where('other_names LIKE ?', f_name)

   LabRequest.joins("INNER JOIN patients ON patients.id = lab_requests.patient_id
                    INNER JOIN users ON users.id = lab_requests.user_id
                    INNER JOIN services_inventories ON services_inventories.id = lab_requests.service_id ").select('service_id,service_name,patients.surname,patients.other_names,patients.mobile_number,patients.card_no,fullname,patients.occupation,patients.address,patients.dob,patients.user_id,lab_type,lab_requests.created_at,lab_requests.patient_id,personnel_id,lab_requests.id').where(delete_status: 0 ).order('id desc'). where('other_names LIKE ?', name)
end




def to_label

  #{self.service_name}  #{self.price}

end

end
