class Patient < ActiveRecord::Base

  validates :surname,  allow_blank: false,:presence => { message: "Please enter the surname"}
  validates :other_names, allow_blank: false,:presence => { message: "Please enter the surname"}
  validates :occupation, allow_blank: false,:presence => { message: "Please enter the occupation"}
  validates :address, allow_blank: false,:presence => { message: "Please enter the address"}
  validates :insurance_cash, allow_blank: false,:presence => { message: "Please Choose either cash or insurance"}
  validates :mobile_number,  allow_blank: false, :presence => { message: "Enter a 12 (233xxxxxxxxx) digit phone number"},
                                        # :numericality => {message: "Enter a 12 (233xxxxxxxxx) digit phone number"},
                                        :length => { :minimum => 10, :maximum => 12 }

  validates_numericality_of   :mobile_number ,
                            :only_integer => true



  validates :surname, format: { with: /\A[a-zA-Z ]+\z/,
    message: "only allows letters" }
  validates :other_names, format: { with: /\A[a-zA-Z ]+\z/,
    message: "only allows letters" }

    validates :dob,  allow_blank: false,:presence => { message: "Please choose Date of birth"}

  belongs_to :a_role, class_name: 'Role', foreign_key: :user_id
  has_many :assigned_patient, class_name: 'AssignedPatient', foreign_key: :patient_id


 def self.per_page
    20
  end
  
def self.patient_no_search(card_no)
  card_no = "%#{card_no}%"
  where('card_no LIKE ?', card_no) 
end


 def self.date_search(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
      end
 end


def self.mobile_number_search(mobile_number)
  mobile_number = "%#{mobile_number}%"
  # where("customer_name LIKE ?",mobile_number)
  where('mobile_number LIKE ?', mobile_number)
end



def self.name_search(name)
  name = "%#{name}%"
  where('surname LIKE ?', name)
end

def self.f_name_search(f_name)
  f_name = "%#{f_name}%"
  where('other_names LIKE ?', f_name)
end



def to_label

  #{self.service_name}  #{self.price}

end


end
