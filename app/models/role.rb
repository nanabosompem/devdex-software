class Role < ActiveRecord::Base

	has_many :users, class_name: 'Users', foreign_key: :role_id
	has_many :patients, class_name: 'Patients', foreign_key: :user_id

end
