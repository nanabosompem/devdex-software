class Category < ActiveRecord::Base

	has_many :service_inventories, class_name: 'ServiceInventory', foreign_key: :category_id
end
