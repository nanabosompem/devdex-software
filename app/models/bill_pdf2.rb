class BillPdf2 
  include RenderAnywhere

  def initialize(bill_history)
    @bill_histories = bill_history
    puts "BILL HISTORIESS in billpdf"
    puts bill_history
    puts bill_history[0].surname

    @total = @bill_histories.sum(:price)
    puts "Total #{@total}"
    puts "------END-------------"
  end

  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A7')
    kit.to_file("#{Rails.root}/public/bill_receipt2.pdf")
  end

  def filename
    "Bill recieipt2.pdf"
  end

  private

 
    attr_reader :bill_history
    

    def as_html
     
      render template: "bill_temps/pdf2", layout: "bill_pdf", locals: { bill_history: @bill_histories, total: @total }
    end
end



