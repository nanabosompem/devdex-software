
class BillPdf 
  include RenderAnywhere

  def initialize(bill_history,total_owe,total_paid)
    @bill_histories = bill_history
    puts "BILL HISTORIESS in billpdf"
    puts bill_history
    puts bill_history[0].surname

    @total_owe = total_owe
    @total_paid = total_paid

    puts "PAID AND OWE in billpdf"
    puts "-----TOTAL PAID #{@total_paid}--------------"
     puts "-----TOTAL OWE #{@total_owe}--------------"

    @total = @bill_histories.sum(:price)
    puts "Total #{@total}"
    puts "------END-------------"
  end

  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/public/bill_receipt.pdf")
  end

  def filename
    "Bill recieipt.pdf"
  end

  private

 
    attr_reader :bill_history
    

    def as_html
     
      render template: "bill_temps/pdf", layout: "bill_pdf", locals: { bill_history: @bill_histories, total_owe: @total_owe,  total_paid: @total_paid }
    end
end



