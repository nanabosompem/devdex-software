class BasicMedRecord < ActiveRecord::Base
  
  def self.patient_no_search(card_no)
  card_no = "%#{card_no}%"
  where('card_no LIKE ?', card_no) 
end


 def self.date_search(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
      end
 end


def self.mobile_number_search(mobile_number)
  mobile_number = "%#{mobile_number}%"
  # where("customer_name LIKE ?",mobile_number)
  where('mobile_number LIKE ?', mobile_number)
end


def self.name_search(name)
  name = "%#{name}%"
  where('surname LIKE ?', name)
end
end
