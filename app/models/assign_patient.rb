class AssignPatient < ActiveRecord::Base



	   belongs_to :patients, class_name: 'Patient', foreign_key: :patient_id
	   belongs_to :a_user, class_name: 'User', foreign_key: :user_id
	   
	   def self.patient_no_search(card_no)
  card_no = "%#{card_no}%"
  # where('card_no LIKE ?', card_no) 

  AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id  INNER JOIN services_inventories ON services_inventories.id  = assign_patients.service_id").select('service_name,surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('card_no LIKE ?', card_no) 
                 
end

	def self.patient_no_search_doc(card_no)
  card_no = "%#{card_no}%"



AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id ").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('card_no LIKE ?', card_no) 
                 
end




 def self.date_search(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
      end
 end


def self.mobile_number_search(mobile_number)
  mobile_number = "%#{mobile_number}%"
  # where("customer_name LIKE ?",mobile_number)
  # where('mobile_number LIKE ?', mobile_number)

   AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id  INNER JOIN services_inventories ON services_inventories.id  = assign_patients.service_id").select('service_name,surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('mobile_number LIKE ?', mobile_number)
  
end


def self.name_search(name)
  name = "%#{name}%"
  # where('surname LIKE ?', name)

  AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id  INNER JOIN services_inventories ON services_inventories.id  = assign_patients.service_id").select('service_name,surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('surname LIKE ?', name)
end

	def self.name_search_doc(name)
  name = "%#{name}%"



AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id ").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('surname LIKE ?', name) 
                 
end


def self.f_name_search(name)
  name = "%#{name}%"
  # where('surname LIKE ?', name)

  AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id  INNER JOIN services_inventories ON services_inventories.id  = assign_patients.service_id").select('service_name,surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('other_names LIKE ?', name)
end



	def self.f_name_search_doc(name)
  name = "%#{name}%"
  
AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id ").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where('other_names LIKE ?', name) 
                 
end


end
