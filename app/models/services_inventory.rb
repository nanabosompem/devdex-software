class ServicesInventory < ActiveRecord::Base

	belongs_to :a_category, class_name: 'Category', foreign_key: :category_id
	
	  require 'csv'
  
 def self.import_data(file,the_type)
  readVal =  file.read

  if the_type == "1" #normal import  
  
     # Thread.new do
      CSV.foreach(file.path, headers: true,encoding: 'iso-8859-1:utf-8') do |row|
        logger.info "RUNNING CSV------------------------------"
        if row["service_name"].present? && row["price"].present? && row["insurance_price"].present? && row["category_id"].present?  && row["quantifiable"].present? 
                @lab_order_list = ServicesInventory.new(service_name: row["service_name"], price: row["price"], insurance_price: row["insurance_price"],category_id: row["category_id"],quantifiable: row["quantifiable"])
                @lab_order_list.save
        end  
      end
  
  end

end  




end
