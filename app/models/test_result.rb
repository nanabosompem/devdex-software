class TestResult < ActiveRecord::Base
  has_attached_file :document
  validates_attachment :document, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }

  # validates :results, presence: true , :presence => { message: "Enter results"}
# 
  # validates :document, presence: true

  def self.per_page
    20
  end
  
def self.patient_no_search(card_no)
  card_no = "%#{card_no}%"


    @test_results = TestResult.joins("INNER JOIN patients ON patients.id = test_results.patient_id
                    INNER JOIN users ON users.id = test_results.user_id INNER JOIN lab_requests ON lab_requests.id = test_results.lab_id INNER JOIN services_inventories ON services_inventories.id = test_results.service_id").select('test_results.document_file_name,service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,results,test_results.patient_id,test_results.created_at,test_results.lab_id,test_results.id').order('id desc').where('card_no LIKE ?', card_no) 
end


 def self.date_search(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
      end
 end


def self.mobile_number_search(mobile_number)
  mobile_number = "%#{mobile_number}%"
  # where("customer_name LIKE ?",mobile_number)




    @test_results = TestResult.joins("INNER JOIN patients ON patients.id = test_results.patient_id
                    INNER JOIN users ON users.id = test_results.user_id INNER JOIN lab_requests ON lab_requests.id = test_results.lab_id INNER JOIN services_inventories ON services_inventories.id = test_results.service_id").select('test_results.document_file_name,service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,results,test_results.patient_id,test_results.created_at,test_results.lab_id,test_results.id').order('id desc').where('mobile_number LIKE ?', mobile_number)
end


def self.name_search(name)
  name = "%#{name}%"

   @test_results = TestResult.joins("INNER JOIN patients ON patients.id = test_results.patient_id
                    INNER JOIN users ON users.id = test_results.user_id INNER JOIN lab_requests ON lab_requests.id = test_results.lab_id INNER JOIN services_inventories ON services_inventories.id = test_results.service_id").select('test_results.document_file_name,service_name,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,lab_type,results,test_results.patient_id,test_results.created_at,test_results.lab_id,test_results.id').order('id desc').where('surname LIKE ?', name)
end

end
