Rails.application.routes.draw do
  resources :fluids
  resources :nurse_notes
  resources :treatment_sheets
  resources :nurse_procedures
  resources :incident_reports
  resources :radio_test_results
  resources :lab_order_lists
  resources :medication_temps
  resources :appointments
  resources :order_requests
  resources :order_masters
  # resources :bill_temps
  resources :bill_temps do
    resource :download, only: [:show] 
    resource :download2, only: [:show] 
  end
  resources :admissions
  resources :service_inventory_histories
  resources :bill_histories
  resources :categories
  resources :services_inventories
  resources :test_results
  resources :radiology_requests
  resources :medication_requests
  resources :lab_requests
  resources :clinic_documents
  resources :basic_med_records
  resources :assign_patients
  resources :patients
  resources :roles
  devise_for :users
  get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  get '/users' => 'users#index'

  get '/users/new' => 'users#new', :as => 'new_user'
  get '/users/:id/edit' => 'users#edit', :as => 'edit_user'
  get '/users/:id' => 'users#show', :as => 'user'
  patch '/users/:id' => 'users#update', :as => 'update_user'
  patch '/patients/:id' => 'patients#update', :as => 'update_patient'
 #patch '/clinic_documents/:id' => 'clinic_documents#update', :as => 'update_clinic_documents'
  post 'create_user' => 'users#create', as: :create_user

  post      'update_clinic_documents'                          =>     'clinic_documents#update_clinic_documents'

  get       'remove_contact'                          =>     'users#remove_contact'

  get       'patients_form'                          =>     'patients#patients_form'
  get  'show_patients'          =>     'patients#show_patients'
  get  'edit_patients'          =>     'patients#edit_patients'
  get  'edit_docs'          =>     'assign_patients#edit_docs'
  get  'assign_a_patient'          =>     'patients#assign_a_patient'
  get  'basic_medical'          =>     'assign_patients#basic_medical'
  get  'assign_a_patient2'          =>     'basic_med_records#assign_a_patient2'


  get  'procedure_list'          =>     'assign_patients#procedure_list'

  get 'view_procedure_record' =>     'nurse_procedures#view_procedure_record'



  get 'incident_form'  =>  'incident_reports#incident_form'
  get 'view_report'  =>  'incident_reports#view_report'
  

  get  'clinic_documentation'          =>     'assign_patients#clinic_documentation'
    get  'appt_documentation'          =>     'appointments#appt_documentation'
  get  'view_documentation'          =>     'assign_patients#view_documentation'
  get  'assign_lab'          =>     'assign_patients#assign_lab'
  get  'assign_med'          =>     'assign_patients#assign_med'
  get  'show_details'          =>     'assign_patients#show_details'
  get  'docs'          =>     'assign_patients#docs'
  get  'lab_docs'          =>     'lab_requests#lab_docs'
  get  'lab_form'          =>     'lab_requests#lab_form'
  get  'view_lab'          =>     'lab_requests#view_lab'
  get  'delete_lab_record'          =>     'lab_requests#delete_lab_record'
  get  'med_docs'          =>     'medication_requests#med_docs'
  get  'med_form'          =>     'medication_requests#med_form'
  get  'view_med'          =>     'medication_requests#view_med'
  get  'other_med'          =>     'medication_requests#other_med'
  get  'other_radio'          =>     'radiology_requests#other_radio'

  get  'radio_docs'          =>     'radiology_requests#radio_docs'
  get  'radio_form'          =>     'radiology_requests#radio_form'
  get  'view_radio'          =>     'radiology_requests#view_radio'
  get 'delete_radio_record'  =>     'radiology_requests#delete_radio_record'


  get  'other_lab'          =>     'lab_requests#other_lab'
  get  'lab_history'          =>     'lab_requests#lab_history'
  get  'enter_results'          =>     'lab_requests#enter_results'
  get  'lab_medical_records'          =>     'lab_requests#lab_medical_records'
  get  'enter_radio_results'   =>     'radiology_requests#enter_radio_results'
  get  'results'          =>     'test_results#results'
  get  'patient_admission'          =>     'admissions#patient_admission'
  get  'admission_doc'          =>     'admissions#admission_doc'
  get 'patient_bill'    => 'bill_histories#patient_bill'
  get 'bill_open'    => 'bill_temps#bill_open'
  get 'outstanding'   => 'bill_temps#outstanding'
  get 'paid'    => 'bill_temps#paid'
  get 'delete_bill_record'    => 'bill_temps#delete_bill_record'

  
  get 'view_note'  => 'admissions#view_note'
  get 'view_discharge_note' => 'admissions#view_discharge_note'
  get  'view_nurse_record'          =>     'basic_med_records#view_nurse_record'
  post 'assign_to_a_doctor '  =>  'assign_patients#assign_to_a_doctor'
  get  'main_test'          =>     'test_results#main_test'
  get  'indi_radio_result'          =>     'radio_test_results#indi_radio_result'
  

  get 'close_request' =>  'medication_requests#close_request'
  get 'open_request'  =>   'medication_requests#open_request'

  get 'close_request_rad' =>  'radiology_requests#close_request_rad'
  get 'open_request_rad'  =>   'radiology_requests#open_request_rad'

  get  'order_records'          =>     'order_requests#order_records'
  get  'make_order'          =>     'order_requests#make_order'
  get  'view_order'          =>     'order_requests#view_order'

  get 'delete_order_record' =>     'order_requests#delete_order_record'

  get 'add_room'    =>      'admissions#add_room'
  get 'edit_room'    =>      'admissions#edit_room'
  post 'add_a_room'    =>      'admissions#add_a_room'

  get 'discharge_patient'    =>      'admissions#discharge_patient'
  get 'discharged_list'    =>      'admissions#discharged_list'
  get 'discharged_list_nurses'    =>      'admissions#discharged_list_nurses'
  get 'nurse_admission_list' => 'admissions#nurse_admission_list'

  get 'admission_vitals' => 'admissions#admission_vitals'
  get 'discharge_note' => 'admissions#discharge_note'
  post 'a_discharge_note' => 'admissions#a_discharge_note'

  get 'nurse_admission_vitals_history' =>  'basic_med_records#nurse_admission_vitals_history'
  get 'admission_vital_graph' =>  'basic_med_records#admission_vital_graph'
  get  'admission_basic_medical'          =>     'basic_med_records#admission_basic_medical'

  get  'doctor_admission_vitals_doc'          =>     'admissions#doctor_admission_vitals_doc'
  get  'view_doctor_admission_doc'          =>     'admissions#view_doctor_admission_doc'

  get 'close_order'  =>   'order_requests#close_order'

  get 'patient_appointment' => 'appointments#patient_appointment'

  get 'reception_patient' => 'patients#reception_patient'
  post 'reception_appointment' => 'appointments#reception_appointment'
  get 'doctor_appointment' => 'appointments#doctor_appointment'

  get 'open_med_temp'  => 'medication_temps#open_med_temp'
  get 'close_med_request'   => 'medication_temps#close_med_request'
  get 'delete_med_record'   => 'medication_temps#delete_med_record'

  get 'print_bill'   => 'bill_temps#print_bill'
  get 'print_all_bill'   => 'bill_temps#print_all_bill'

  post 'basic_vitals' => 'assign_patients#basic_vitals'

  get 'import_csv_file' => 'services_inventories#import_csv_file', as: :import_csv_file
  post 'import' => 'services_inventories#import', as: :import
  
    
  
  #new routes for viewing nurse vitals
   get  'view_vitals'          =>     'assign_patients#view_vitals'


   #ROutes to templates
   get 'print_labresults'   => 'lab_requests#print_labresults'
   get 'print_pharmacy'   => 'bill_temps#print_labresults'
   get 'print_xray'   => 'bill_temps#print_labresults'
   get 'print_ultrasound'   => 'bill_temps#print_labresults'

   #view recipts'
   get 'view_reciept'   => 'bill_temps#view_reciept'

   #For Set orders
   get 'set_orders'   => 'home#set_orders'


   get 'patient_profile'   => 'patients#patient_profile'


    get 'patient_treatment' => 'treatment_sheets#patient_treatment'
   get 'new_treatment' => 'treatment_sheets#new_treatment'


   get 'nurses_note' => 'basic_med_records#nurses_note'


   get 'new_nurse_note' => 'basic_med_records#new_nurse_note'
   get 'open_note' => 'basic_med_records#open_note'


   get 'fluids_intake' => 'fluids#fluids_intake'

   get 'new_fluids_intake' => 'fluids#new_fluids_intake'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end
end
