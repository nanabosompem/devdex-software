class CreateFluids < ActiveRecord::Migration
  def change
    create_table :fluids do |t|
      t.text :orals
      t.integer :orals_amount
      t.string :iv_fluids
      t.integer :fluids_amount
      t.string :urine_amount
      t.string :emesis_amount
      t.string :drainage_amount
      t.integer :user_id
      t.datetime :time
      t.integer :patient_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end
