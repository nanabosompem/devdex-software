class CreateNurseProcedures < ActiveRecord::Migration
  def change
    create_table :nurse_procedures do |t|
      t.integer :service_id
      t.integer :patient_id
      t.boolean :status
      t.text :complaints

      t.timestamps null: false
    end
  end
end
