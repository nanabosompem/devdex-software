class CreateTreatmentSheets < ActiveRecord::Migration
  def change
    create_table :treatment_sheets do |t|
      t.string :drugs
      t.integer :user_id
      t.datetime :time
      t.boolean :status

      t.timestamps null: false
    end
  end
end
