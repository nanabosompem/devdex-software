class CreateNurseNotes < ActiveRecord::Migration
  def change
    create_table :nurse_notes do |t|
      t.text :notes
      t.integer :user_id
      t.datetime :time
      t.integer :patient_id
      t.boolean :status

      t.timestamps null: false
    end
  end
end
