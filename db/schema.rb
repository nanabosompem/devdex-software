# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200505100315) do

  create_table "admissions", force: :cascade do |t|
    t.integer  "patient_id",     limit: 4
    t.string   "admission_note", limit: 255
    t.integer  "user_id",        limit: 4
    t.integer  "personnel_id",   limit: 4
    t.boolean  "status"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "room_no",        limit: 255
    t.text     "discharge_note", limit: 65535
    t.integer  "admission_days", limit: 4
  end

  create_table "appointments", force: :cascade do |t|
    t.string   "description", limit: 255
    t.datetime "app_time"
    t.boolean  "status"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "doctor_id",   limit: 255
    t.string   "color",       limit: 255
    t.string   "patient_id",  limit: 255
  end

  create_table "assign_patients", force: :cascade do |t|
    t.integer  "patient_id",              limit: 4
    t.integer  "role_id",                 limit: 4
    t.boolean  "status"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "personnel_id",            limit: 4
    t.string   "user_id",                 limit: 255
    t.integer  "basic_medical_record_id", limit: 4
    t.string   "service_id",              limit: 255
  end

  create_table "basic_med_records", force: :cascade do |t|
    t.integer  "patient_id",       limit: 4
    t.string   "blood_pressure",   limit: 255
    t.string   "pulse",            limit: 255
    t.string   "temp",             limit: 255
    t.string   "respiratory_rate", limit: 255
    t.integer  "user_id",          limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "status"
    t.text     "complaints",       limit: 65535
    t.boolean  "admission_status"
    t.string   "saturation",       limit: 255
    t.string   "weight",           limit: 255
    t.string   "height",           limit: 255
  end

  create_table "bill_histories", force: :cascade do |t|
    t.integer  "service_id",    limit: 4
    t.integer  "patient_id",    limit: 4
    t.float    "price",         limit: 24
    t.boolean  "status"
    t.integer  "quantity",      limit: 4
    t.decimal  "unit_price",                precision: 10
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "lab_order_id",  limit: 255
    t.boolean  "delete_status"
  end

  create_table "bill_temps", force: :cascade do |t|
    t.integer  "patient_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.boolean  "status"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "category_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "clinic_documents", force: :cascade do |t|
    t.integer  "patient_id",              limit: 4
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "user_id",                 limit: 4
    t.boolean  "status"
    t.text     "complaints",              limit: 65535
    t.text     "symptoms",                limit: 65535
    t.text     "vital_signs",             limit: 65535
    t.text     "clinic_exams",            limit: 65535
    t.text     "final_diagnosis",         limit: 65535
    t.text     "investigations",          limit: 65535
    t.text     "treatment_plans",         limit: 65535
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.text     "med_history",             limit: 65535
    t.text     "social_history",          limit: 65535
    t.text     "allergies",               limit: 65535
    t.text     "history_complaints",      limit: 65535
    t.text     "direct_question",         limit: 65535
    t.text     "systems_review",          limit: 65535
  end

  create_table "fluids", force: :cascade do |t|
    t.text     "orals",           limit: 65535
    t.integer  "orals_amount",    limit: 4
    t.string   "iv_fluids",       limit: 255
    t.integer  "fluids_amount",   limit: 4
    t.string   "urine_amount",    limit: 255
    t.string   "emesis_amount",   limit: 255
    t.string   "drainage_amount", limit: 255
    t.integer  "user_id",         limit: 4
    t.datetime "time"
    t.integer  "patient_id",      limit: 4
    t.boolean  "status"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "incident_reports", force: :cascade do |t|
    t.text     "incident_type",        limit: 65535
    t.date     "incidate_date"
    t.string   "incident_description", limit: 255
    t.text     "action_taken",         limit: 65535
    t.boolean  "status"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_id",              limit: 4
  end

  create_table "lab_order_lists", force: :cascade do |t|
    t.string   "lab_name",   limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "lab_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "lab_type",                limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "service_id",              limit: 4
    t.string   "lab_order_id",            limit: 255
    t.string   "document_file_name",      limit: 255
    t.string   "document_content_type",   limit: 255
    t.integer  "document_file_size",      limit: 4
    t.datetime "document_updated_at"
    t.boolean  "delete_status"
  end

  create_table "medication_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "medication",              limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "service_id",              limit: 4
  end

  create_table "medication_temps", force: :cascade do |t|
    t.string   "patient_id", limit: 255
    t.string   "service_id", limit: 255
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "nurse_notes", force: :cascade do |t|
    t.text     "notes",      limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "time"
    t.integer  "patient_id", limit: 4
    t.boolean  "status"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "nurse_procedures", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.integer  "patient_id", limit: 4
    t.boolean  "status"
    t.text     "complaints", limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id",    limit: 4
  end

  create_table "order_masters", force: :cascade do |t|
    t.string   "order_name", limit: 255
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "order_requests", force: :cascade do |t|
    t.integer  "patient_id",      limit: 4
    t.integer  "user_id",         limit: 4
    t.integer  "personnel_id",    limit: 4
    t.boolean  "status"
    t.integer  "order_master_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "service_id",      limit: 255
    t.integer  "drug_quantity",   limit: 4
    t.text     "dosage",          limit: 65535
    t.boolean  "delete_status"
  end

  create_table "patients", force: :cascade do |t|
    t.string   "title",                limit: 255
    t.string   "surname",              limit: 255
    t.string   "other_names",          limit: 255
    t.string   "mobile_number",        limit: 255
    t.string   "occupation",           limit: 255
    t.string   "address",              limit: 255
    t.boolean  "status"
    t.integer  "user_id",              limit: 4
    t.string   "alt_number",           limit: 255
    t.date     "dob"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "card_no",              limit: 255
    t.string   "gender",               limit: 10
    t.string   "religion",             limit: 25
    t.string   "emergency_c_name",     limit: 255
    t.string   "emergency_c_number",   limit: 255
    t.string   "insurance_cash",       limit: 255
    t.string   "refers",               limit: 255
    t.boolean  "assigned_status"
    t.boolean  "pay_status"
    t.string   "email",                limit: 255
    t.string   "emergency_c_location", limit: 255
    t.string   "emergency_c_email",    limit: 255
    t.string   "marital_status",       limit: 255
  end

  create_table "radio_test_results", force: :cascade do |t|
    t.integer  "patient_id",            limit: 4
    t.integer  "user_id",               limit: 4
    t.boolean  "status"
    t.string   "results",               limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "lab_id",                limit: 4
    t.integer  "service_id",            limit: 4
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
    t.integer  "radio_id",              limit: 4
  end

  create_table "radiology_requests", force: :cascade do |t|
    t.integer  "basic_medical_record_id", limit: 4
    t.integer  "patient_id",              limit: 4
    t.integer  "user_id",                 limit: 4
    t.integer  "personnel_id",            limit: 4
    t.boolean  "status"
    t.string   "radiology",               limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "service_id",              limit: 4
    t.string   "document_file_name",      limit: 255
    t.string   "document_content_type",   limit: 255
    t.integer  "document_file_size",      limit: 4
    t.datetime "document_updated_at"
    t.boolean  "delete_status"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "role_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "service_inventory_histories", force: :cascade do |t|
    t.integer  "service_id", limit: 4
    t.decimal  "price",                precision: 10
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "services_inventories", force: :cascade do |t|
    t.string   "service_name",    limit: 255
    t.float    "price",           limit: 24
    t.integer  "category_id",     limit: 4
    t.boolean  "quantifiable"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.float    "insurance_price", limit: 24
  end

  create_table "test_results", force: :cascade do |t|
    t.integer  "patient_id",            limit: 4
    t.integer  "user_id",               limit: 4
    t.boolean  "status"
    t.string   "results",               limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "lab_id",                limit: 4
    t.integer  "service_id",            limit: 4
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
  end

  create_table "treatment_sheets", force: :cascade do |t|
    t.string   "drugs",      limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "time"
    t.boolean  "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "username",               limit: 255
    t.string   "fullname",               limit: 255
    t.string   "mobile_number",          limit: 255
    t.integer  "role_id",                limit: 4
    t.boolean  "status"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
